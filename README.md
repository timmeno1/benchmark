# README #

## Help ##
    -h, --help               output usage information
    -V, --version            output the version number
    -d, --directory <value>  choose testing directory
    -f, --file <files>       choose files in directory "tests" to test (use "," as split between files)
    -i, --iteration <val>    select number of iteration, default = 1000000


## Usage ##

Test all files in directory you need


```
#!bash

$ node index.js -d /path/to/your/dir -i 1000000000
```




Test several files in directory ./tests

```
#!bash

$ node index.js -f file1.js,file2.js,file3.js - i 1000000000
```


## Description to test results ##
**You can add description to testing files:**

```
#!javascript

...
var description = 'some description';
...
```