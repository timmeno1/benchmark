let EventEmmiter = require('events');
let cp = require('child_process');
let fs = require('fs');
let Table = require('cli-table2');

class Tester extends EventEmmiter{
    constructor(input, iter){
        super();
        this.input = input;
        this.iter = iter;
        this.re = /.js$/;
        this.files = [];
        this.readyFiles = [];
        this.children = [];
        this.fullResult = [];

        this.on('maketest', ()=>{
            this.makeTest(this.files);
        });
        this.on('fullResult', ()=>{
            this.drawTable(this.fullResult)
        });
        this.on('runtest', ()=>{
            this.test(this.readyFiles);
        });
    }



    makeTest(files){
        let testContentStart = "var pusage = require('pidusage');\n" +
            "var startTime = Date.now();\n" +
            "var endTime = null;\n" +
            "var resTime = null\n" +
            "var description = 'n/a' \n" +
            `for(var i=0;i<${this.iter};i++){\n`;

        let testContentEnd = "\n}\n" +
            "pusage.stat(process.pid, function (err, stat) {\n"+
            "    var  memVal = '';\n"+
            "    if(stat.memory/1000000000 >= 1 ){\n"+
            "        stat.memory = stat.memory/1000000000;\n"+
            "        memVal = ' GB';\n"+
            "    }\n"+
            "    if(stat.memory/1000000 >= 1 ){\n"+
            "        stat.memory = stat.memory/1000000;\n"+
            "        memVal = ' MB';\n"+
            "    }\n"+
            "    if(stat.memory/1000 >= 1 ){\n"+
            "        stat.memory = stat.memory/1000;\n"+
            "        memVal = ' KB';\n"+
            "    }\n"+
            "    endTime = Date.now();\n"+
            "    resTime = (endTime - startTime)/1000;\n"+
            "    process.send({" +
            "       cpu: `${stat.cpu.toFixed(2)} %`," +
            "       mem: `${stat.memory.toFixed(2)}${memVal}`," +
            "       time: `${resTime.toFixed(2)} sec`," +
            "       desc: description" +
            "    });\n"+
            "});\n" +
            "pusage.unmonitor(process.pid);\n";

        for(let i=0;i<files.length;i++){
            let test_content = fs.readFileSync(files[i]).toString();
            test_content = testContentStart + test_content + testContentEnd;
            fs.writeFileSync(files[i] + '_test', test_content);
            this.readyFiles.push(files[i] + '_test');
            if(i==files.length-1) {
                this.emit('runtest');
            }
        }


    }
    receive(child, file){
        return new Promise((resolve,reject)=>{
            child.on('message', (m)=> {
                m.file = file;
                resolve(m);
            });
            child.on('error', (err)=>{
                console.log(err);
            });
        });
    }

    async test(input){
        for(let i=0;i<input.length;i++){
            this.children.push(cp.fork(input[i]));
            let result = await this.receive(this.children[i], input[i]);
            this.fullResult.push(result);
            fs.unlinkSync(input[i])
            if(i==input.length-1){
                this.emit('fullResult');
            }
        }
    }

    drawTable(fullResult){
        let table = new Table({
            chars: { 'top': '═' , 'top-mid': '╤' , 'top-left': '╔' , 'top-right': '╗'
                , 'bottom': '═' , 'bottom-mid': '╧' , 'bottom-left': '╚' , 'bottom-right': '╝'
                , 'left': '║' , 'left-mid': '╟' , 'mid': '─' , 'mid-mid': '┼'
                , 'right': '║' , 'right-mid': '╢' , 'middle': '│' }
        });
        table.push(["Tested file", "CPU usage", "Memory usage", 'Time spent', 'Desciption'] );

        for (let i = 0; i<fullResult.length; i++){
            let testedFile = fullResult[i].file;
            let cpu = fullResult[i].cpu;
            let mem = fullResult[i].mem;
            let time = fullResult[i].time;
            let description = fullResult[i].desc;

            table.push(
                [testedFile, cpu, mem, time, description]
            );
        }
        console.log(table.toString());
    }

    runDir(){
        let files = fs.readdirSync(this.input);
        for(let i=0;i<files.length;i++){
            if(i==files.length-1){
                if(this.re.test(files[i])) {
                    this.files.push(this.input + '/' + files[i]);
                }
                    this.emit('maketest');
                }else{
                    if(this.re.test(files[i])) {
                        this.files.push(this.input + '/' + files[i]);
                    }
                }
            }
        }


    runFiles(){
        let files = this.input;
        for(let i=0;i<files.length;i++){
            if(i==files.length-1){
                if(this.re.test(files[i])) {
                    this.files.push('tests/' + files[i]);
                }
                this.emit('maketest');
            }else{
                if(this.re.test(files[i])) {
                    this.files.push('tests/' + files[i]);
                }
            }
        }
    }
}


module.exports = Tester;


