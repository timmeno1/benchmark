let program = require('commander');
//let os = process.platform;
//let fs = require('fs');
//let cp = require('child_process');
//let Table = require('cli-table2');
let iterations = 1000000000;
let Tester = require('./lib/tester.js');



function list(val) {
    return val.split(',');
}

function value(val){
    return val;
}



program
    .version('0.0.13')
    //  choose directory, default
    .option('-d, --directory <value>', 'choose testing directory', value)
    // choose file
    .option('-f, --file <files>', 'choose files in directory "tests" to test (use "," as split between files)', list)
    .option('-i, --iteration <val>', 'select number of iteration, default = 1000000', value)
    .parse(process.argv); //program.args
if(!program.iteration){
    program.iteration = 1000000;
}
if (program.directory){
    let tester = new Tester(program.directory, program.iteration);
    tester.runDir()
}else if (program.file){
    let tester = new Tester(program.file, program.iteration);
    tester.runFiles();
}

