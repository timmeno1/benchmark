'use strict';

function drawingTable(finishedResult) {
    var table = new Table({
        chars: { 'top': '═', 'top-mid': '╤', 'top-left': '╔', 'top-right': '╗',
            'bottom': '═', 'bottom-mid': '╧', 'bottom-left': '╚', 'bottom-right': '╝',
            'left': '║', 'left-mid': '╟', 'mid': '─', 'mid-mid': '┼',
            'right': '║', 'right-mid': '╢', 'middle': '│' }
    });
    table.push(["Tested file", "CPU usage", "Memory usage", 'Time spent', 'Desciption']);

    for (var i = 0; i < finishedResult.length; i++) {
        var testedFile = finishedResult[i].file;
        var cpu = finishedResult[i].cpu;
        var mem = finishedResult[i].mem;
        var time = finishedResult[i].time;
        var description = finishedResult[i].desc;

        table.push([testedFile, cpu, mem, time, description]);
    }
    console.log(table.toString());
}

function makeTest(input_file, output_file) {

    var testContentStart = "var pusage = require('pidusage');\n" + "var startTime = Date.now();\n" + "var endTime = null;\n" + "var resTime = null\n" + "var description = 'n/a' \n" + `for(var i=0;i<${ iterations };i++){\n   `;

    var testContentEnd = "\n}\n" + "pusage.stat(process.pid, function (err, stat) {\n" + "    var  memVal = '';\n" + "    if(stat.memory/1000000000 >= 1 ){\n" + "        stat.memory = stat.memory/1000000000;\n" + "        memVal = ' GB';\n" + "    }\n" + "    if(stat.memory/1000000 >= 1 ){\n" + "        stat.memory = stat.memory/1000000;\n" + "        memVal = ' MB';\n" + "    }\n" + "    if(stat.memory/1000 >= 1 ){\n" + "        stat.memory = stat.memory/1000;\n" + "        memVal = ' KB';\n" + "    }\n" + "    endTime = Date.now();\n" + "    resTime = (endTime - startTime)/1000;\n" + "    process.send({" + "       cpu: `${stat.cpu.toFixed(2)} %`," + "       mem: `${stat.memory.toFixed(2)}${memVal}`," + "       time: `${resTime.toFixed(2)} sec`," + "       desc: description" + "    });\n" + "});\n" + "pusage.unmonitor(process.pid);\n";

    test_content = fs.readFileSync(input_file).toString();
    test_content = testContentStart + test_content + testContentEnd;
    fs.writeFileSync(output_file, test_content);
    runTest(output_file);
}

function runTest(file) {

    var child = cp.fork(file);
    child.on('message', m => {
        m.file = file;
        finishedResult.push(m);
    });
    child.on('exit', () => {
        fs.unlinkSync(file);
    });
}

if (program.directory) {
    (function () {
        var re = /.js$/;
        var files = fs.readdirSync(program.directory);
        files.forEach((cur, index, arr) => {
            if (re.test(cur)) {
                cur = program.directory + '/' + cur;
                makeTest(cur, cur + '_test');
            }
        });
        drawingTable(finishedResult);
    })();
}